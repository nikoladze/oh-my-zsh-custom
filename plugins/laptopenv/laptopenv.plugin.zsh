
# setupRoot () {
#     pushd /home/nikolai/programme/root-6.06.00
#     source bin/thisroot.sh
#     popd
# }

# alias sr=setupRoot

export PATH=/home/nikolai/bin:$PATH
#export PYTHONPATH=/home/nikolai/python:$PYTHONPATH
export PATH=/home/nikolai/.local/bin:$PATH
#source /opt/miniconda3/etc/profile.d/conda.sh

export RUCIO_HOME=$HOME/.local

alias ssh="TERM=xterm-256color ssh"
