# environment variables
export PATH=${HOME}/bin:$PATH
export PATH=${HOME}/.autojump/bin:$PATH
export PATH=${HOME}/.local/bin:$PATH
export PYTHONPATH=${HOME}/python:$PYTHONPATH
export MANPATH=${HOME}/man:$MANPATH

# needed for tmux to spawn correctly via ssh
export TMPDIR=/tmp

# make emacs tmp dir ... wtf
[[ -d /tmp/emacs12255 ]] || mkdir /tmp/emacs12255
chmod 700 /tmp/emacs12255

# garching specific aliases
alias gpg=gpg2

# maybe better for NFS dirs?
# https://unix.stackexchange.com/questions/162078/stop-zsh-from-completing-parent-directories
zstyle ':completion:*' path-completion false
zstyle ':completion:*' accept-exact-dirs true

setupConda() {
    eval "$(~/miniconda3/bin/conda shell.zsh hook)"
    unset PYTHONPATH
    export PYTHONNOUSERSITE=True
}

setupXRD() {
    export LD_LIBRARY_PATH=/home/n/Nikolai.Hartmann/xrootd/v4.11.3/lib
    export PATH=$HOME/xrootd/v4.11.3/bin:$PATH
}

# --------------------------------------------------
# SLC/centos with singularity
# --------------------------------------------------

# centos7, slc6 commands to load singularity image
source /project/etpsw/Common/bin/setup_image.sh

if [[ -n "$SINGULARITY_CONTAINER" ]]
then
    HISTFILE=~/.zsh_history
fi


MYDIST=""
case $(cat /etc/issue) in
    *Ubuntu*)
        MYDIST="Ubuntu";;
    *Scientific*)
        MYDIST="SLC"
esac

case $SINGULARITY_IMAGE in
    *centos7*)
        MYDIST="C7"
esac

case $SINGULARITY_NAME in
    *centos7*)
        MYDIST="C7"
esac


#always setup ATLAS in SLC
if [[ "$MYDIST" = "SLC" ]] || [[ ("$MYDIST" = "C7") ]]
then

    dist_info="%{$fg_bold[red]%}("$MYDIST")"

    # doesn't seem to work
    # autoload bashcompinit
    # bashcompinit

    # # manually set completion for some stuff
    # complete -f -W 'grid_submit_nobuild get_release set_release tag_package get_all_ldflags download_file strip download tag_log test_ut make_par get_ldflags checkout_pkg set_field get_dependency get_cxxflags version external_link grid_test clean_pkg get_field checkout get_location external_compile status make_skeleton test_cc grid_submit update package_list clean_dep make_doxygen build find_packages du grid_compile_nobuild external_download check_dep svn_retry compile clean compile_pkg root make_bin_area' rc
    # complete -W ' agis asetup atlantis boost davix dq2 eiclient emi fax fftw ganga gcc gccxml gsl lcgenv pacman panda pod pyami python rcsetup root rucio sft xrootd' lsetup

    # source cmt.zsh
    # complete -o default -F _cmt cmt

    # for schroot and atlas software + other things to be executed
    # --------------------------------------------------
    #setupATLAS
fi

setupATLAS_c7 () {
    # this setup script still has it's problems ...

    # use singularity from cvmfs
    export PATH=/cvmfs/atlas.cern.ch/repo/containers/sw/singularity/x86_64-el7/current/bin/:$PATH

    pwd_old=$PWD

    # not sure if it helps to set this already here ...
    export LC_ALL="en_US.UTF-8"

    # see https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers
    setupATLAS -c centos7+batch --nohome \
               -e "-B /home:/home -B /project:/project -B /scratch-local:/scratch-local" \
               --postsetup "export HOME=$HOME;cd ${pwd_old};source $HOME/.zshrc"
}

# --------------------------------------------------
# Modules
# --------------------------------------------------

[[ "$MYDIST" = "Ubuntu" ]] && source /etc/profile.d/modules.sh 2> /dev/null

# adapted from /etc/profile.d/modules.sh
# initialize module system
# (the permissions for the completion function files are messed up, so this currently doesn't work)
# modules_version="4.2.5"
# zsh_source=/software/opt/noarch/Modules/$modules_version/init/zsh
# [[ -f $zsh_source ]] && source $zsh_source
# module unuse /software/opt/noarch/Modules/$modules_version/modulefiles
# export MODULES_BASE=/software/opt
# export MODULES_DIST=bionic
# export MODULES_ARCH=x86_64
# export MODULES_HELPERS=/software/opt/modulefiles

# # use only module files available of the current distribution and architecture
# module unuse /software/opt/modulefiles
# module use /software/opt/modulefiles-by-dist/bionic/x86_64
# module use /software/opt/modulefiles-by-dist/noarch

# load my current favorite root/python version
#module load root/6.18.04
#module unload marabou
#module load root/6.20.04_py3.7
#module load python/3.7-2019.07

# the python module seems to modify $FPATH
# if not exported it will be overwritten and mess up zsh
#export FPATH=$FPATH

# other option: reset back
# does not solve all problems, eg zsh still messed up in centos7 image
# -> only load these modules when needed
module-wtf () {
    FPATH_old=$FPATH
    fpath_old=($fpath)
    module $@
    unset FPATH
    FPATH=$FPATH_old
    fpath=($fpath_old)
}
alias mlp="module-wtf load python"
alias mlr="module-wtf unload root && module-wtf load root/6.20.04_py3.7"

setup_smartbkg() {
    export PATH=/project/agkuhr/users/nhartmann/software/micromamba/envs/smartbkg/bin:$PATH
    source ~/venv/smartbkg-micromamba/bin/activate
}

export MAMBA_ROOT_PREFIX=/scratch-local/nhartmann/software/micromamba
eval "$(~/bin/micromamba shell hook -s posix)"

export BELLE2_CONFIG_DIR=/project/agkuhr/belle2
export FZF_BASE=/home/n/Nikolai.Hartmann/code/fzf
