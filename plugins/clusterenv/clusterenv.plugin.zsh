# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias gpg='gpg2'

setupBELLE2 () {
  SYSTEM=ubuntu1604
  LATEST=$(cd /cvmfs/belle.cern.ch/${SYSTEM}/releases; ls -dv1 release-* | tail -n1)
  RELEASE=${1:-${LATEST}}

  source /cvmfs/belle.cern.ch/${SYSTEM}/tools/b2setup
  b2setup ${RELEASE}
}
