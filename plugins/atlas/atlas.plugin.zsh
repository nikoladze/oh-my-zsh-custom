export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
export RUCIO_ACCOUNT=nihartma
export RUCIO_HOME=$HOME/.local

# prompt when using singularity image
MYDIST=""
case $SINGULARITY_NAME in
    *centos7*)
        MYDIST="C7"
esac
if [[ "$MYDIST" = "C7" ]]
then
    dist_info="%{$fg_bold[red]%}("$MYDIST")"
fi

