zstyle ':completion:*' file-sort time

# history options
setopt    APPEND_HISTORY
setopt    HIST_IGNORE_ALL_DUPS
setopt    HIST_IGNORE_DUPS

# show autocomplete options instead of pasting all
setopt GLOB_COMPLETE

export TF_FORCE_GPU_ALLOW_GROWTH=true

# https://github.com/junegunn/fzf/wiki/Examples#integration-with-autojump
j() {
    local preview_cmd="ls {2..}"
    if command -v exa &> /dev/null; then
        preview_cmd="exa -l {2}"
    fi

    if [[ $# -eq 0 ]]; then
                 cd "$(autojump -s | sort -k1gr | awk -F : '$1 ~ /[0-9]/ && $2 ~ /^\s*\// {print $1 $2}' | fzf --height 40% --reverse --inline-info --preview "$preview_cmd" --preview-window down:50% | cut -d$'\t' -f2- | sed 's/^\s*//')"
    else
        cd $(autojump $@)
    fi
}
