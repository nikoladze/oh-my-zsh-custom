alias mute="amixer sset Master toggle"
alias unmute="
amixer sset Master unmute;
amixer sset Headphone unmute;
amixer sset Speaker unmute;
amixer sset PCM unmute;
amixer sset Speaker 95%;
amixer sset Master 30%;
amixer sset Headphone 95%;
amixer sset PCM 95%
"
alias neo="setxkbmap de neo"
alias fail="win"
alias ew="evince -w"
alias en="nohup evince"
alias blq="bc -lq ~/.bcrc"
alias rtl="ls -rtlh"
alias eq="\emacs -nw -q"
alias rtld="ls -rtld */"
alias gdc="git diff --cached --color"
alias em="emacsclient -n"
alias et="emacsclient -t"
alias t="tmux"
alias bash="bash --init-file ~/.bashrc_nozsh"
alias fpm="firefox -no-remote -ProfileManager"
ev() {
    (evince "$@" > /dev/null &)
}
alias trw="tmux rename-window"
alias trp='tmux rename-window $(basename $(pwd))'
#alias ud='unset DBUS_SESSION_BUS_ADDRESS'
alias exd='export DISPLAY=":0"'
alias rl="readlink -f"
alias gis="git status"
alias gdw="git diff --word-diff"
alias gcw="git diff --cached --word-diff"
alias suicide="killall -u $(whoami)"
alias voms="voms-proxy-init -voms atlas --valid 96:0"
alias wdiff="git --no-pager diff --word-diff=plain --no-index"
alias as="autojump -s"
alias setupHF="source ~/da/scripts/setupHF.sh"
alias setupHF60="source ~/da/scripts/setupHF60.sh"
alias rsynclrz='rsync -avuz --exclude "*.git" --exclude "*.svn" --exclude "RootCoreBin" --exclude "InstallArea" --exclude "WorkArea"'
alias setupSlurm='source ~/da/scripts/setupSlurm.sh'
alias wsl='watch_slurm.sh'
alias rn='~/scripts/renumber.sh'
alias lsd='ls -d'
alias ppp="ppPickle.py"
alias ppj="ppJSON.py"
alias ppc="ppCSV.py"
alias sec="source ~/p/scripts/setupConda.sh"
alias acs="autocutsel -fork & autocutsel -selection PRIMARY -fork &"
red () {
cmd=$1
[[ "$cmd" = "bull" ]] && cmd="pull"
[[ "$cmd" = "bush" ]] && cmd="push"
git $cmd
}
gif-init () {
    fname=$1
    gitname=".git-$fname"
    git init --bare "$gitname"
    echo "*" >> "$gitname/info/exclude"
    echo "!$fname" >> "$gitname/info/exclude"
}
gif () {
    fname=$1
    shift
    gitname=".git-$fname"
    git --git-dir="$gitname" --work-tree=. "$@"
}
alias light="alacritty-colorscheme -C ~/.eendroroy-alacritty-theme/themes apply base16-tomorrow-256.yaml"
alias dark="alacritty-colorscheme -C ~/.eendroroy-alacritty-theme/themes apply tomorrow_night.yaml"
alias ..="cd .."
mount_cvmfs () {
    for d in /cvmfs/*
    do
        sudo mount -t cvmfs $(basename $d) $d
    done
}
umount_cvmfs () {
    for d in /cvmfs/*
    do
        sudo umount $d
    done
}
setupVOMS() {
    local_user_proxy=$HOME/x509up_u$(id -u)
    tmp_user_proxy=/tmp/x509up_u$(id -u)
    if [[ -f "${tmp_user_proxy}" ]]
    then
        rm -f ${local_user_proxy}
        cp ${tmp_user_proxy} ${local_user_proxy}
    fi
    if [[ -f "$local_user_proxy" ]]
    then
        chmod 400 $local_user_proxy
        export X509_USER_PROXY=$local_user_proxy
    else
        echo "${local_user_proxy} not found"
    fi
    export X509_VOMS_DIR=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/emi/4.0.3-1_191004/etc/grid-security/vomsdir
    local_certificate_dir=$HOME/.local/etc/grid-security/certificates
    if [[ -d "${local_certificate_dir}" ]]
    then
        export X509_CERT_DIR=${local_certificate_dir}
    else
        export X509_CERT_DIR=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/etc/grid-security-emi/certificates
    fi
}
alias ip="ip --color"
ssh-forward() {
    ssh -L ${1}:localhost:${1} -N -f ${2}
}
alias pu="pueue"
alias guno="gis -uno"
